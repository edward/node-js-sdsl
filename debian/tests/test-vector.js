const assert1 = require('assert');
const Vector = require('js-sdsl').Vector;

var v1 = new Vector();
var t1 = "test123";
var t2 = "test321";
var d1;

/* push data */
v1.pushBack(t1);
v1.pushBack(t2);

assert1.equal(v1.size(), 2, "length should be 2");

d1 = v1.back();
v1.popBack();

assert1.equal(v1.size(), 1, "length should be 1");

assert1.equal(d1, t2, "data pushed to vector is not equal");

d1 = v1.back();
v1.popBack();

assert1.equal(v1.size(), 0, "length should be 0");

assert1.equal(d1, t1, "data pushed to vector is not equal");

