const assert1 = require('assert');
const Set = require('js-sdsl').Set;

var s1 = new Set();
var t1 = "test123";
var t2 = "test321";
var t3 = "test333";

/* insert data */
s1.insert(t1);
s1.insert(t2);

assert1(s1.find(t1), "t1 should be in set");
assert1(s1.find(t2), "t2 should be in set");
assert1(!s1.find(t3), "t3 should not be in set");
